<?php

// Kickstart the framework
$f3=require('lib/base.php');

$f3->set('DEBUG',1);
$f3->set('TITLE','F3 BoilerPlate');
if ((float)PCRE_VERSION<7.9)
	trigger_error('PCRE version is out of date');

// Load configuration
$f3->config('config.ini');
$f3->route('GET /',
	function($f3) {
		$f3->set('content','welcome.htm');
		echo View::instance()->render('layout.htm');
	}
);
$f3->set('ONERROR',function($f3){
	$f3->set('content','error.htm');
	echo View::instance()->render('layout.htm');
});
$f3->run();
